# Sigrok session combiner

This project written in C combines multiple srzip file format generated by PulseView into a single srzip file. It combines all data signal that are specified in srzip files.

# Compile

```bash
make
```

# How to use

Put all srzip files that you want to combine in the same folder as the combine executable and run it:

```bash
./combine
```

If you want to add time shifts to any of sigrok sessions, you can create a file named `timeshift` (configurable in `main.h`). 

For example if you want to apply a time shift to a specific srzip file, such as`capture.sr` , write the following in the `timeshift` file:

```
capture=32.954n
```

You can also specify negative time shifts like this:

```
capture=-32.954n
```

You can use m, u, n, p, or f for prefix, although it seems like PulseView can't show lower than 100 ps.

## Output
It will create a folder named `final` (configurable in `main.h`) and a corresponding srzip file in the same directory as the utility. The final folder will also contain the unzipped contents. The data signals in the `final` sr file will be renamed in the format: **"original_filename"-"original_data_probe_name"**.

# Limitations
- Assumes all srzip files in the provided folder were generated by PulseView and are compatible using sigrok session file format version 2.

- It only combines logic data and discard any analog data.

- Processes a maximum of `MAX_SR_NUMBER` sigrok sessions (configurable in `main.h`).

- Maximum length of data probe names must be smaller than `MAX_NAME_LENGTH` (configurable in `main.h`).