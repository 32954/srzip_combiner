DIR = src/
SRCS = $(DIR)main.c $(DIR)unzip.c $(DIR)metadata.c $(DIR)logic.c $(DIR)timeshift.c $(DIR)final.c
CFLAGS = -g -Wall -Wextra -pedantic -std=c11
LDLIBS = -lm

combine: $(SRCS) $(DIR)main.h
	gcc $(CFLAGS) -o $@ $(SRCS) $(LDLIBS)

clean:
	rm -f combine
