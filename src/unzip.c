#include "main.h"

void unzip_sr(SrList *list)
{
    int file_count = 0;
    struct dirent *entry;
    char cwd[FILENAME_MAX];
    const char extension[] = ".sr";
    char command[FILENAME_MAX];

    if (getcwd(cwd, sizeof(cwd)) == NULL)
    {
        perror("Error getting current directory");
        exit(EXIT_FAILURE);
    }

    DIR *dir = opendir(cwd);
    if (dir == NULL)
    {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }

    // Extract sr file names and store them in the structure
    while ((entry = readdir(dir)) != NULL)
    {
        const char *file_extension = strrchr(entry->d_name, '.');
        if (file_extension != NULL && strcmp(file_extension, extension) == 0)
        {
            int len = strlen(entry->d_name);
            // Copy the file name without extension into the filenames array
            strncpy(list->filenames[file_count], entry->d_name, len - strlen(extension));
            list->filenames[file_count][len - strlen(extension)] = '\0';
            file_count++;

            if (file_count > MAX_SR_NUMBER)
            {
                fprintf(stderr, "sr files are more than %d\n", MAX_SR_NUMBER);
                exit(EXIT_FAILURE);
            }
        }
    }

    qsort(list->filenames, file_count, MAX_NAME_LENGTH, sortNames);
    list->count = file_count;

    // Extract contents of sr files using the 'unzip' command
    for (int i = 0; i < file_count; i++)
    {
        sprintf(command, "unzip -oq %s.sr -d %s", list->filenames[i], list->filenames[i]);
        printf("%s\n", command);
        if (system(command) == -1)
        {
            perror("Can't unzip");
            exit(EXIT_FAILURE);
        }
    }

    closedir(dir);
}

int sortNames(const void *a, const void *b)
{
    return strcmp((const char *)a, (const char *)b);
}