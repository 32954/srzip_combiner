#include "main.h"

void final_sr_metadata(struct Metadata *final_sr, const SrList *list)
{
    strcpy(final_sr->capturefile, "logic-1");

    for (int i = 0; i < list->count; i++)
    {
        (final_sr->total_dataProbes) += (list->metadatas[i].total_dataProbes);
    }

    final_sr->total_probes = (ceil((final_sr->total_dataProbes) / 8.0)) * 8;

    final_sr->samplerate = list->metadatas[0].samplerate;

    for (int i = 1; i < list->count; i++)
    {
        final_sr->samplerate = lcm((final_sr->samplerate), (list->metadatas[i].samplerate));
    }

    final_sr->unitsize = (final_sr->total_probes) / 8;
}

size_t gcd(size_t a, size_t b)
{
    size_t temp = 0;
    while (b != 0)
    {
        temp = b;
        b = a % b;
        a = temp;
    }
    return a;
}

size_t lcm(size_t a, size_t b)
{
    return (a / gcd(a, b)) * b;
}

void createFinalFolder(void)
{
    struct stat exist;
    if (stat(final, &exist) == 0)
    {
        char command[FILENAME_MAX];
        sprintf(command, "rm -r %s", final);
        if (system(command) == -1)
        {
            perror("Can't remove existing folder.\n");
            exit(EXIT_FAILURE);
        }
        if (mkdir(final, 0777) != 0)
        {
            perror("Error creating folder.\n");
            exit(EXIT_FAILURE);
        }
    }
    else if (mkdir(final, 0777) != 0)
    {
        perror("Error creating folder.\n");
        exit(EXIT_FAILURE);
    }
}

void createFinalVersion(void)
{
    char filePath[FILENAME_MAX];
    snprintf(filePath, sizeof(filePath), "%s/%s", final, "version");

    FILE *file = fopen(filePath, "w");
    if (file == NULL)
    {
        perror("Error opening the file.\n");
        exit(EXIT_FAILURE);
    }

    fprintf(file, "%d", 2);

    fclose(file);
}

void createFinalMetadata(const SrList *list, const struct Metadata *final_sr)
{
    char filePath[FILENAME_MAX];
    snprintf(filePath, sizeof(filePath), "%s/%s", final, "metadata");

    FILE *file = fopen(filePath, "w");
    if (file == NULL)
    {
        perror("Error opening the file.\n");
        exit(EXIT_FAILURE);
    }

    const char *content = "[global]\n"
                          "sigrok version=0.5.1\n\n"
                          "[device 1]\n";

    fputs(content, file);

    fprintf(file, "%s%s\n", "capturefile=", "logic-1");
    fprintf(file, "%s%d\n", "total probes=", final_sr->total_probes);
    fprintf(file, "%s%s\n", "samplerate=", formatFrequency(final_sr->samplerate));
    fprintf(file, "%s\n", "total analog=0");

    char probeName[FILENAME_MAX];
    for (int i = 0, k = 1; i < list->count; i++)
    {
        for (int j = 0; j < list->metadatas[i].total_dataProbes; j++, k++)
        {
            snprintf(probeName, sizeof(probeName), "probe%d=%s-%s", k,
                     list->filenames[i],
                     list->metadatas[i].probes[list->metadatas[i].dataProbes[j]]);
            fprintf(file, "%s\n", probeName);
        }
    }

    fprintf(file, "%s%d\n", "unitsize=", final_sr->unitsize);

    fclose(file);
}

const char *formatFrequency(size_t frequency)
{
    const char *units[] = {"Hz", "kHz", "MHz", "GHz"};
    int maxUnitIdx = sizeof(units) / sizeof(units[0]) - 1;
    int unitIdx = 0;
    double freq = (double)frequency;

    while (freq >= 1000.0 && unitIdx < maxUnitIdx - 1)
    {
        freq /= 1000.0;
        unitIdx++;
    }

    static char result[FILENAME_MAX];
    snprintf(result, sizeof(result), "%.3f", freq);

    int len = strlen(result);

    int remainingSpace = sizeof(result) - len;
    snprintf(result + len, remainingSpace, " %s", units[unitIdx]);

    return result;
}

void moveLogic(void)
{
    char command[FILENAME_MAX];
    sprintf(command, "mv %s %s", "logic-1", final);
    if (system(command) == -1)
    {
        perror("Can't move file.\n");
        exit(EXIT_FAILURE);
    }
}

void zipFinalFolder(void)
{
    char command[FILENAME_MAX];
    sprintf(command, "zip -j %s/%s%s %s/*", final, final, ".sr", final);
    printf("%s\n", command);
    if (system(command) == -1)
    {
        perror("Can't remove existing folder.\n");
        exit(EXIT_FAILURE);
    }
}

void cleanFolders(const SrList *list)
{
    char command[FILENAME_MAX];
    for (int i = 0; i < list->count; i++)
    {
        sprintf(command, "rm -r %s", list->filenames[i]);
        if (system(command) == -1)
        {
            perror("Can't remove existing folder.\n");
            exit(EXIT_FAILURE);
        }
    }
}