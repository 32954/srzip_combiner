#include "main.h"

int main()
{
    SrList list = {0};
    unzip_sr(&list);
    if (list.count == 0)
    {
        printf("There aren't any sr files.\n");
        exit(EXIT_SUCCESS);
    }

    printf("\nNumber of sr files = %d\n\n", list.count);

    list.metadatas = (struct Metadata *)calloc((list.count), sizeof(struct Metadata));
    list.sampleTime = (float *)calloc((list.count), sizeof(float));

    for (int i = 0; i < list.count; i++)
    {
        initializeMetadata(&(list.metadatas[i]));
    }

    metadataExtract(&list);

    struct Metadata final_sr;
    initializeMetadata(&final_sr);

    final_sr_metadata(&final_sr, &list);

    for (int i = 0; i < list.count; i++)
    {
        size_t bytes = combineLogicFiles(list.filenames[i], list.metadatas[i].capturefile);

        list.sampleTime[i] = ((float)(bytes)) / ((float)(list.metadatas[i].samplerate));
    }

    getTimeShift((&list));

    float maxSampleTime = 0.0;

    for (int i = 0; i < list.count; i++)
    {
        if (list.sampleTime[i] > maxSampleTime)
        {
            maxSampleTime = list.sampleTime[i];
        }
    }

    sameSampleRateSet(&list, &final_sr);

    sameSampleTimeSet(&list, &final_sr, maxSampleTime);

    logicShift(&list, &final_sr);

    combineLogics(maxSampleTime, &list, &final_sr);

    createFinalFolder();
    createFinalVersion();
    createFinalMetadata(&list, &final_sr);
    moveLogic();
    zipFinalFolder();
    cleanFolders(&list);

    for (int i = 0; i < list.count; i++)
    {
        free(list.metadatas[i].probes);
        free(list.metadatas[i].dataProbes);
    }
    free(list.metadatas);
    free(list.sampleTime);

    return 0;
}
