#include "main.h"

void metadataExtract(SrList *list)
{
    char line[FILENAME_MAX];
    char filePath[FILENAME_MAX];
    FILE *file = NULL;

    for (int i = 0; i < list->count; i++)
    {
        snprintf(filePath, sizeof(filePath), "%s/%s", list->filenames[i], "metadata");

        file = fopen(filePath, "r");
        if (file == NULL)
        {
            perror("Failed to open metadata.\n");
            exit(EXIT_FAILURE);
        }

        while (fgets(line, sizeof(line), file))
        {
            line[strcspn(line, "\n")] = 0;
            extractData(line, &(list->metadatas[i]));
        }

        fclose(file);
    }
}

void extractData(char *line, struct Metadata *metadata)
{
    char key[100], value[100], samplerate_value[MAX_NAME_LENGTH];
    if (sscanf(line, "%[^=]=%[^\n]", key, value) == 2)
    {
        if (strcmp(key, "capturefile") == 0)
        {
            strcpy(metadata->capturefile, value);
        }
        else if (strcmp(key, "total probes") == 0)
        {
            sscanf(value, "%d", &metadata->total_probes);
            metadata->total_probes = (ceil((metadata->total_probes) / 8.0)) * 8;

            metadata->probes = (char(*)[MAX_NAME_LENGTH])malloc((metadata->total_probes) * MAX_NAME_LENGTH * sizeof(char));
            metadata->dataProbes = (int *)malloc((metadata->total_probes) * sizeof(int));

            if ((metadata->probes == NULL) || (metadata->dataProbes == NULL))
            {
                perror("Failed to allocate memory for probes.\n");
                exit(1);
            }
        }
        else if (strcmp(key, "samplerate") == 0)
        {
            strcpy(samplerate_value, value);
            metadata->samplerate = convertToHertz(samplerate_value);
        }
        else if (strstr(key, "probe") == key)
        {
            int probe_number = 0;
            sscanf(key, "probe%d", &probe_number);
            strcpy(metadata->probes[probe_number - 1], value);
            metadata->dataProbes[metadata->total_dataProbes] = (probe_number - 1);
            ++(metadata->total_dataProbes);
        }
        else if (strcmp(key, "unitsize") == 0)
        {
            sscanf(value, "%d", &metadata->unitsize);
        }
    }
}

size_t convertToHertz(const char *inputStr)
{
    size_t value = 0;
    char frequencyStr[4];

    if (sscanf(inputStr, "%zu %3s", &value, frequencyStr) != 2)
    {
        fprintf(stderr, "Invalid input format: %s\n", inputStr);
        exit(1);
    }

    size_t frequencyMultiplier = convertFrequency(frequencyStr);

    return value * frequencyMultiplier;
}

size_t convertFrequency(char *frequencyStr)
{
    if (strcmp(frequencyStr, "Hz") == 0)
    {
        return 1LL;
    }
    else if (strcmp(frequencyStr, "kHz") == 0)
    {
        return 1000LL;
    }
    else if (strcmp(frequencyStr, "MHz") == 0)
    {
        return 1000000LL;
    }
    else if (strcmp(frequencyStr, "GHz") == 0)
    {
        return 1000000000LL;
    }
    else
    {
        fprintf(stderr, "Invalid frequency: %s\n", frequencyStr);
        exit(1);
    }
}

void initializeMetadata(struct Metadata *metadata)
{
    memset(metadata->capturefile, 0, sizeof(metadata->capturefile));
    metadata->total_probes = 0;
    metadata->samplerate = 0;
    metadata->probes = NULL;
    metadata->total_dataProbes = 0;
    metadata->dataProbes = NULL;
    metadata->unitsize = 0;
    metadata->shift = 0.0;
}