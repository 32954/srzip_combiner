#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define MAX_SR_NUMBER 24
#define MAX_NAME_LENGTH 50

struct Metadata
{
    char capturefile[MAX_NAME_LENGTH];
    int total_probes;
    size_t samplerate;
    char (*probes)[MAX_NAME_LENGTH];
    int unitsize;
    int total_dataProbes;
    int *dataProbes;
    float shift;
};

typedef struct
{
    int count;
    char filenames[MAX_SR_NUMBER][MAX_NAME_LENGTH];
    float *sampleTime;
    struct Metadata *metadatas;
} SrList;

///////////////////////////////////////////////////////////////////////////////
// unzip.c
///////////////////////////////////////////////////////////////////////////////

// Get file names and their corresponding numbers and unzip them
void unzip_sr(SrList *list);

// Sort sigrok session files in the 'filenames' array of SrList structure
int sortNames(const void *a, const void *b);

///////////////////////////////////////////////////////////////////////////////
// metadata.c
///////////////////////////////////////////////////////////////////////////////

// Extract data from metadata of all sigrok session files
void metadataExtract(SrList *list);

// Extract data from the line to store it in the structure
void extractData(char *line, struct Metadata *metadata);

// Function to convert the formatted string to Hertz
size_t convertToHertz(const char *inputStr);

// Convert a frequency value from string to a numeric representation in hertz
size_t convertFrequency(char *frequencyStr);

// Initialize Metadata structure
void initializeMetadata(struct Metadata *metadata);

///////////////////////////////////////////////////////////////////////////////
// logic.c
///////////////////////////////////////////////////////////////////////////////

// File contain all logic files if there are multiple of them in a sr file
#define combinedLogics "combine"

// Logic file for syncing logics sample rate
#define msr "matchedSampleRate"

// Logic file for syncing logics of different sr files
#define matchedLogics "matchedLogics"

// Combine multiple logic files, such as logic-1-1, logic-1-2, ...
size_t combineLogicFiles(const char *directoryPath, const char *capturefile);

// Adjust the data in the logic files to align with the LCM sample rate.
void sameSampleRateSet(const SrList *list, const struct Metadata *final_sr);

// Append data to the end of logic files to ensure they share the same maximum
// sampling time. The appended data will match the value of the last sample.
void sameSampleTimeSet(const SrList *list, const struct Metadata *final_sr, float maxSampleTime);

// Shift sigrok sessions based on values inside timeShiftFile
// Add zero to the beginning of logics if shift is positive
void logicShift(const SrList *list, const struct Metadata *final_sr);

// Merge all sr logic files into a single 'logic-1' file.
void combineLogics(float maxSampleTime, const SrList *list, struct Metadata *final_sr);

typedef struct
{
    char *filename;
    char *data;
    size_t size;
} mappedFile;

// Map file into memory
mappedFile mapFile(char *filename);

// Unmap a previously mapped file from memory
void unmapFile(mappedFile *file);

// Insert data at the end of the array and shifting existing elements
void insertChar(char array[], int size, char data);

///////////////////////////////////////////////////////////////////////////////
// timeshift.c
///////////////////////////////////////////////////////////////////////////////

// File contain shift times for each sigrok session
#define timeShiftFile "timeshift"

// Read timeShiftFile for shift values and put them in Srlist structure
void getTimeShift(SrList *list);

// Sort names inside timeShiftFile for comparison with 'filenames' array of SrList structure
void sortTimeShiftFile(const char *filepath);

// Set shift values from timeShiftFile in Srlist structure
void extractTimeShift(char *line, SrList *list);

// Convert shift string to seconds
float convertToSeconds(const char *shift);

///////////////////////////////////////////////////////////////////////////////
// final.c
///////////////////////////////////////////////////////////////////////////////

// final folder and sr name
#define final "final"

// Calculate final sr file metadata content
void final_sr_metadata(struct Metadata *final_sr, const SrList *list);

// Calculate the Least Common Multiple (LCM)
size_t lcm(size_t a, size_t b);

// Calculate the Greatest Common Divisor (GCD)
size_t gcd(size_t a, size_t b);

// Create the final folder to put final sigrok session and its content
void createFinalFolder(void);

// Create the final sigrok session version file
void createFinalVersion(void);

// Create the final sigrok session metadata file
void createFinalMetadata(const SrList *list, const struct Metadata *final_sr);

// Return the frequency as a string in an appropriate unit (Hz, kHz, MHz or GHz)
const char *formatFrequency(size_t frequency);

// Move final 'logic-1' file to final folder
void moveLogic(void);

// Create final sigrok session file (.sr)
void zipFinalFolder(void);

// Remove extracted sigrok sessions
void cleanFolders(const SrList *list);