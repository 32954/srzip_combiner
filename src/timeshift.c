#include "main.h"

void getTimeShift(SrList *list)
{
    char line[FILENAME_MAX];
    char filePath[FILENAME_MAX];
    FILE *file = NULL;

    snprintf(filePath, sizeof(filePath), "%s", timeShiftFile);

    file = fopen(filePath, "r");
    if (file == NULL)
    {
        perror("Error opening file.\n");
        exit(EXIT_FAILURE);
    }

    sortTimeShiftFile(filePath);

    while (fgets(line, sizeof(line), file))
    {
        line[strcspn(line, "\n")] = 0;
        extractTimeShift(line, list);
    }

    fclose(file);
}

void sortTimeShiftFile(const char *filepath)
{
    char command[FILENAME_MAX];
    sprintf(command, "sort %s > sorted.tmp && mv sorted.tmp %s", filepath, filepath);
    if (system(command) == -1)
    {
        perror("Can't sort file.\n");
        exit(EXIT_FAILURE);
    }
}

void extractTimeShift(char *line, SrList *list)
{
    char name[100], shift[100];
    float sign = 0.0;

    if (sscanf(line, "%[^=]=-%[^\n]", name, shift) == 2)
    {
        sign = -1.0;
    }
    else if (sscanf(line, "%[^=]=%[^\n]", name, shift) == 2)
    {
        sign = 1.0;
    }
    else
    {
        perror("time shift format is wrong.\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < list->count; i++)
    {
        if (strcmp(list->filenames[i], name) == 0)
        {
            list->metadatas[i].shift = convertToSeconds(shift);
            list->metadatas[i].shift *= sign;
        }
    }
}

float convertToSeconds(const char *shift)
{
    float multiplier = 1.0;

    if (strstr(shift, "f") != NULL)
    {
        multiplier = 1e-15;
    }
    else if (strstr(shift, "p") != NULL)
    {
        multiplier = 1e-12;
    }
    else if (strstr(shift, "n") != NULL)
    {
        multiplier = 1e-9;
    }
    else if (strstr(shift, "u") != NULL)
    {
        multiplier = 1e-6;
    }
    else if (strstr(shift, "m") != NULL)
    {
        multiplier = 1e-3;
    }

    float timeInSeconds = strtod(shift, NULL);

    timeInSeconds *= multiplier;

    return timeInSeconds;
}