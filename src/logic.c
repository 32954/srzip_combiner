#include "main.h"

size_t combineLogicFiles(const char *directoryPath, const char *capturefile)
{
    size_t totalBytes = 0;

    char filePath[FILENAME_MAX];
    sprintf(filePath, "%s/%s", directoryPath, combinedLogics);

    FILE *file = fopen(filePath, "wb");
    if (file == NULL)
    {
        perror("Error creating the logic file.\n");
        exit(EXIT_FAILURE);
    }

    DIR *directory = opendir(directoryPath);
    if (directory == NULL)
    {
        perror("Error opening the directory.\n");
        fclose(file);
        exit(EXIT_FAILURE);
    }

    struct dirent *dirEntry;
    int logicNumber = 1;
    while ((dirEntry = readdir(directory)) != NULL)
    {
        if (strncmp(dirEntry->d_name, capturefile, 7) == 0)
        {
            char logicFilePath[FILENAME_MAX];
            sprintf(logicFilePath, "%s/%s-%d", directoryPath, capturefile, logicNumber);
            ++logicNumber;

            FILE *logicFile = fopen(logicFilePath, "rb");
            if (logicFile == NULL)
            {
                fprintf(stderr, "Error opening the file: %s\n", logicFilePath);
                fclose(file);
                closedir(directory);
                exit(EXIT_FAILURE);
            }

            char buffer[1024];
            size_t bytesRead;

            while ((bytesRead = fread(buffer, 1, sizeof(buffer), logicFile)) > 0)
            {
                totalBytes += bytesRead;
                fwrite(buffer, 1, bytesRead, file);
            }

            fclose(logicFile);
        }
    }

    closedir(directory);
    fclose(file);
    return totalBytes;
}

void sameSampleRateSet(const SrList *list, const struct Metadata *final_sr)
{
    FILE *file;
    FILE *out;

    char logicFilePath[FILENAME_MAX];
    char sameSampleFilePath[FILENAME_MAX];
    for (int i = 0; i < list->count; i++)
    {
        snprintf(logicFilePath, sizeof(logicFilePath), "%s/%s", list->filenames[i], combinedLogics);
        snprintf(sameSampleFilePath, sizeof(sameSampleFilePath), "%s/%s", list->filenames[i], msr);

        file = fopen(logicFilePath, "rb+");

        if (file == NULL)
        {
            perror("Error opening file");
            exit(EXIT_FAILURE);
        }

        out = fopen(sameSampleFilePath, "wb+");

        if (out == NULL)
        {
            perror("Error opening file");
            fclose(file);
            exit(EXIT_FAILURE);
        }

        size_t arraySize = list->metadatas[i].unitsize;
        char *byte = (char *)calloc(arraySize, sizeof(char));
        size_t diffSampleRate = (final_sr->samplerate) / (list->metadatas[i].samplerate);

        while (fread(byte, sizeof(char), arraySize, file) == arraySize)
        {
            for (size_t j = 0; j < diffSampleRate; j++)
            {
                fwrite(byte, sizeof(char), arraySize, out);
            }
        }

        free(byte);
        fclose(file);
        fclose(out);
    }
}

void sameSampleTimeSet(const SrList *list, const struct Metadata *final_sr, float maxSampleTime)
{
    char sameSampleFilePath[FILENAME_MAX];
    char matchedLogicsPath[FILENAME_MAX];
    FILE *sameSampleFile = NULL;
    FILE *matchedLogicsFile = NULL;
    size_t samples = 0;
    char *lastSample = NULL;
    int bytes = 0;

    for (int i = 0; i < list->count; i++)
    {
        snprintf(sameSampleFilePath, sizeof(sameSampleFilePath), "%s/%s", list->filenames[i], msr);
        snprintf(matchedLogicsPath, sizeof(matchedLogicsPath), "%s/%s", list->filenames[i], matchedLogics);

        sameSampleFile = fopen(sameSampleFilePath, "rb+");
        if (sameSampleFile == NULL)
        {
            perror("Error opening the file");
            exit(EXIT_FAILURE);
        }

        matchedLogicsFile = fopen(matchedLogicsPath, "wb+");
        if (matchedLogicsFile == NULL)
        {
            perror("Error opening the file");
            exit(EXIT_FAILURE);
        }

        samples = (size_t)round((maxSampleTime - (list->sampleTime[i])) * (final_sr->samplerate) * (list->metadatas[i].unitsize));

        bytes = ((list->metadatas[i].total_probes) / 8);

        fseek(sameSampleFile, -bytes, SEEK_END);

        lastSample = (char *)calloc(bytes, sizeof(char));

        fread(lastSample, sizeof(char), bytes, sameSampleFile);

        fseek(sameSampleFile, 0, SEEK_END);

        size_t fileSize = ftell(sameSampleFile);
        fseek(sameSampleFile, 0, SEEK_SET);

        char *buffer = (char *)malloc(fileSize);
        if (buffer == NULL)
        {
            perror("Memory allocation failed");
            free(lastSample);
            fclose(sameSampleFile);
            fclose(matchedLogicsFile);
            exit(EXIT_FAILURE);
        }

        fread(buffer, 1, fileSize, sameSampleFile);
        fwrite(buffer, 1, fileSize, matchedLogicsFile);

        for (size_t j = 0; j < samples; j++)
        {
            fwrite(lastSample, sizeof(char), bytes, matchedLogicsFile);
        }

        free(buffer);
        free(lastSample);
        fclose(sameSampleFile);
        fclose(matchedLogicsFile);
    }
}

void logicShift(const SrList *list, const struct Metadata *final_sr)
{
    for (int i = 0; i < list->count; i++)
    {
        if ((list->metadatas[i].shift) != 0.0)
        {
            char matchedLogicsPath[FILENAME_MAX];
            char tempFilePath[FILENAME_MAX];

            FILE *matchedLogicsFile, *tempFile;

            snprintf(matchedLogicsPath, sizeof(matchedLogicsPath), "%s/%s", list->filenames[i], matchedLogics);
            snprintf(tempFilePath, sizeof(matchedLogicsPath), "%s/%s", list->filenames[i], "temp");

            matchedLogicsFile = fopen(matchedLogicsPath, "rb");
            if (matchedLogicsFile == NULL)
            {
                perror("Error opening file");
                exit(EXIT_FAILURE);
            }

            tempFile = fopen(tempFilePath, "wb");
            if (tempFile == NULL)
            {
                perror("Error opening file");
                exit(EXIT_FAILURE);
            }

            size_t samples = (fabs(list->metadatas[i].shift)) * (final_sr->samplerate) * (list->metadatas[i].unitsize);

            if ((list->metadatas[i].shift) > 0)
            {
                char zero = 0;

                for (size_t j = 0; j < samples; j++)
                {
                    fwrite(&zero, sizeof(char), 1, tempFile);
                }

                char buffer[1024];
                size_t bytesRead;

                while ((bytesRead = fread(buffer, 1, sizeof(buffer), matchedLogicsFile)) > 0)
                {
                    fwrite(buffer, 1, bytesRead, tempFile);
                }

                fclose(matchedLogicsFile);
                fclose(tempFile);

                remove(matchedLogicsPath);

                if (rename(tempFilePath, matchedLogicsPath) != 0)
                {
                    perror("Error renaming file");
                    exit(EXIT_FAILURE);
                }
            }
            else if ((list->metadatas[i].shift) < 0)
            {
                fseek(matchedLogicsFile, samples, SEEK_SET);

                char buffer[1024];
                size_t bytesRead;

                while ((bytesRead = fread(buffer, 1, sizeof(buffer), matchedLogicsFile)) > 0)
                {
                    fwrite(buffer, 1, bytesRead, tempFile);
                }

                fclose(matchedLogicsFile);
                fclose(tempFile);

                remove(matchedLogicsPath);

                if (rename(tempFilePath, matchedLogicsPath) != 0)
                {
                    perror("Error renaming file");
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
}

void combineLogics(float maxSampleTime, const SrList *list, struct Metadata *final_sr)
{

    FILE *top_logic;

    top_logic = fopen("logic-1", "wb");
    if (top_logic == NULL)
    {
        perror("Error opening the file.\n");
        exit(EXIT_FAILURE);
    }

    char matchedLogicsPath[FILENAME_MAX];
    mappedFile mapped_files[list->count];

    for (int i = 0; i < list->count; i++)
    {
        snprintf(matchedLogicsPath, sizeof(matchedLogicsPath), "%s/%s", list->filenames[i], matchedLogics);
        mapped_files[i] = mapFile(matchedLogicsPath);
    }

    char sample = 0;
    int sampleCount = 0;
    int bit = 0;
    char top_sample[(final_sr->total_probes) / 8];
    memset(top_sample, 0, (final_sr->total_probes) / 8);

    for (size_t n = 0; n < (size_t)round(maxSampleTime * (final_sr->samplerate)); n++)
    {
        for (int i = 0; i < list->count; i++)
        {
            int jump = list->metadatas[i].unitsize;
            char *fileSample = (char *)calloc(jump, sizeof(char));

            memcpy(fileSample, ((mapped_files[i].data) + n * jump), jump);

            for (int k = 0; k < (list->metadatas[i].total_dataProbes); k++)
            {
                bit = ((fileSample[k / 8]) >> ((list->metadatas[i].dataProbes[k]) % 8)) & 1;

                sample |= (bit << (sampleCount));
                sampleCount++;
                if (sampleCount == 8)
                {
                    insertChar(top_sample, (final_sr->total_probes) / 8, sample);
                    sampleCount = 0;
                    sample = 0;
                }
            }
            free(fileSample);
        }
        if (sampleCount != 0)
        {
            for (size_t i = sampleCount; i < 8; i++)
            {
                bit = 0;
                sample |= (bit << (sampleCount));
                sampleCount++;
                if (sampleCount == 8)
                {
                    insertChar(top_sample, (final_sr->total_probes) / 8, sample);
                    sampleCount = 0;
                    sample = 0;
                }
            }
        }
        fwrite(top_sample, sizeof(char), (final_sr->total_probes) / 8, top_logic);
        memset(top_sample, 0, (final_sr->total_probes) / 8);
    }
    fclose(top_logic);
    unmapFile(mapped_files);
}

mappedFile mapFile(char *filename)
{
    mappedFile file;
    file.filename = filename;
    file.data = NULL;
    file.size = 0;

    int fd = open(filename, O_RDONLY);
    if (fd == -1)
    {
        perror("Error opening file");
        return file;
    }

    struct stat st;
    if (fstat(fd, &st) == -1)
    {
        perror("Error getting file size");
        close(fd);
        return file;
    }

    file.size = st.st_size;
    file.data = (char *)mmap(NULL, file.size, PROT_READ, MAP_SHARED, fd, 0);
    close(fd);

    if (file.data == MAP_FAILED)
    {
        perror("Error mapping file to memory");
        file.data = NULL;
        file.size = 0;
    }

    return file;
}

void unmapFile(mappedFile *file)
{
    if (file->data != NULL)
    {
        if (munmap(file->data, file->size))
        {
            perror("Error unmapping file from memory");
        }
        file->data = NULL;
        file->size = 0;
    }
}

void insertChar(char array[], int size, char data)
{
    for (int i = 0; i < size; i++)
    {
        array[i] = array[i + 1];
    }
    array[size - 1] = data;
}